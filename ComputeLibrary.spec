%define so_ver 35
# Disable validation tests by default due to opencl needing to be set up
%bcond_with computelibrary_tests
Name:           ComputeLibrary
Version:        24.02
Release:        1.0
Summary:        Arm Compute Library
License:        MIT
URL:            https://developer.arm.com/technologies/compute-library
Source:         https://github.com/ARM-software/ComputeLibrary/archive/v%{version}.tar.gz#/ComputeLibrary-%{version}.tar.gz
BuildRequires:  gcc-c++
BuildRequires:  git-core
BuildRequires:  scons >= 2.4
Recommends:     %{name}-sample-data
ExclusiveArch:  aarch64 x86_64

%description
The Arm Compute Library is a collection of low-level machine learning functions optimized for Cortex-A CPU, Neoverse and Mali GPU architectures. The Arm Compute Library provides superior performance to other open source alternatives and immediate support for new Arm technologies e.g. SVE2
Examples binaries part.

%package -n libarm_compute%{so_ver}
Summary:        Arm Compute Library

%description -n libarm_compute%{so_ver}
The Arm Compute Library is a collection of low-level machine learning functions optimized for Cortex-A CPU, Neoverse and Mali GPU architectures. The Arm Compute Library provides superior performance to other open source alternatives and immediate support for new Arm technologies e.g. SVE2
Library part.

%package -n libarm_compute_graph%{so_ver}
Summary:        Arm Compute Library - Graph part

%description -n libarm_compute_graph%{so_ver}
The Arm Compute Library is a collection of low-level machine learning functions optimized for Cortex-A CPU, Neoverse and Mali GPU architectures. The Arm Compute Library provides superior performance to other open source alternatives and immediate support for new Arm technologies e.g. SVE2
Library part.

%package devel
Summary:        Arm Compute Library -- devel
Requires:       %{name} = %{version}
Requires:       libarm_compute%{so_ver} = %{version}
Requires:       libarm_compute_graph%{so_ver} = %{version}

%description devel
The Arm Compute Library is a collection of low-level machine learning functions optimized for Cortex-A CPU, Neoverse and Mali GPU architectures. The Arm Compute Library provides superior performance to other open source alternatives and immediate support for new Arm technologies e.g. SVE2
Devel part, including headers.

%package sample-data
%define sampledir sample-data
Summary:        Compute Library sample data

%description sample-data
Free *.npy and *.ppm files to use with example binaries.

%prep
%setup -q -n ComputeLibrary-%{version}

%build
scons os=linux build=native \
      set_soname=1 \
      examples=1 \
      opencl=1 \
      debug=0 asserts=0 \
      embed_kernels=1 \
%if %{with computelibrary_tests}
      validation_tests=1 \
%else
      validation_tests=0 \
%endif
%ifarch aarch64
      multi_isa=1 \
      neon=1 arch=arm64-v8a \
%endif
%ifarch x86_64
      neon=0 arch=x86_64 \
%endif
      extra_cxx_flags="%{optflags}" \
      Werror=0 %{?_smp_mflags}

%install
rm build/examples/*.o
rm build/examples/gemm_tuner/*.o
mv build/examples/gemm_tuner/* build/examples
rm -r build/examples/gemm_tuner
mkdir -p %{buildroot}%{_bindir}
install -Dm0755 build/examples/* %{buildroot}%{_bindir}/
mkdir -p %{buildroot}%{_libdir}
cp -a build/*.so* %{buildroot}%{_libdir}/
mkdir -p %{buildroot}%{_includedir}/
cp -a arm_compute/ include/half/ include/libnpy/ support/ utils/ %{buildroot}%{_includedir}/
# Remove *.cpp files from includedir
rm -f $(find %{buildroot}%{_includedir}/ -name *.cpp)
# Install sample data
mkdir -p %{buildroot}%{_datadir}/ComputeLibrary/%{sampledir}
cp -r data/* %{buildroot}%{_datadir}/ComputeLibrary/%{sampledir}
# Install scripts
rm -rf scripts/modules # Unused scripts/module/Shell.py
install -Dm0755 scripts/{*.py,*.go,*.sh} %{buildroot}%{_bindir}
# Fix Python scripts interpreter
for pyfile in `ls %{buildroot}%{_bindir}/*.py`; do
  sed -i -e 's|#!%{_bindir}/env python3|#!%{_bindir}/python3|' $pyfile
  sed -i -e 's|#!%{_bindir}/env python|#!%{_bindir}/python3|' $pyfile
done
sed -i -e 's|#!%{_bindir}/python|#!%{_bindir}/python3|' %{buildroot}%{_bindir}/generate_build_files.py

%post -n libarm_compute%{so_ver} -p /sbin/ldconfig
%postun -n libarm_compute%{so_ver} -p /sbin/ldconfig

%post -n libarm_compute_graph%{so_ver} -p /sbin/ldconfig
%postun -n libarm_compute_graph%{so_ver} -p /sbin/ldconfig

%if %{with computelibrary_tests}
%check
LD_LIBRARY_PATH="build/" build/tests/arm_compute_validation
%endif

%files
%{_bindir}/*

%files -n libarm_compute%{so_ver}
%license LICENSE
%{_libdir}/*.so.%{so_ver}*
%files -n libarm_compute_graph%{so_ver}
%license LICENSE
%{_libdir}/libarm_compute_graph.so.%{so_ver}*

%files devel
%dir %{_includedir}/arm_compute
%dir %{_includedir}/half
%dir %{_includedir}/libnpy
%dir %{_includedir}/support
%dir %{_includedir}/utils
%{_includedir}/arm_compute/*
%{_includedir}/half/*
%{_includedir}/libnpy/*
%{_includedir}/support/*
%{_includedir}/utils/*
%{_libdir}/*.so

%files sample-data
%dir %{_datadir}/ComputeLibrary/
%{_datadir}/ComputeLibrary/%{sampledir}

%changelog
* Thu Mar 14 2024 Jun He <jun.he@arm.com> - 24.02-1.0
- update version to 24.02

* Fri Dec 23 2022 Dandan Xu <dandan@nj.iscas.ac.cn> - 22.11-1.1
- update version to 22.11

* Mon Jun 22 2020 Senlin Xia <xisenlin1@huawei.com> - 20.02.1-2
- fix build errors

* Mon May 10 2010 sinever <sinever@126.com>
- Initial release 20.02.1
